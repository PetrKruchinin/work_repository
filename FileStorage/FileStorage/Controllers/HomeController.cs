﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FileStorage.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace FileStorage.Controllers
{
    public class HomeController : Controller
    {
       
        ApplicationContext _context;
        IWebHostEnvironment _appEnvironment;
        List<string> filesList;
        List<FileInfo> fileInfos;

        public HomeController(ApplicationContext context, IWebHostEnvironment appEnvironment)
        {
            
            _context = context;
            _appEnvironment = appEnvironment;

            filesList = new List<string>();
            fileInfos = new List<FileInfo>();
            string[] fileArray = Directory.GetFiles(_appEnvironment.WebRootPath + "/Files/");
            foreach (string file in fileArray)
            {
                filesList.Add(Path.GetFileName(file));
                var FileInfo = new FileInfo(file);
                fileInfos.Add(FileInfo);
            }


        }
        public IActionResult Index()
        {
            ViewBag.filesList = filesList;
            ViewBag.fileInfos = fileInfos;
            return View(_context.Files.ToList());   
        }
        [HttpPost]
        public async Task<IActionResult> AddFile(IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                // путь к папке Files
                string path = "/Files/" + uploadedFile.FileName;
                // сохраняем файл в папку Files в каталоге wwwroot
                /*Для получения полного пути к каталогу wwwroot use свойство WebRootPath объекта IWebHostEnvironment.
                * Для копирования файла в папку Files use поток FileStream, в который записывается файл с помощью метода CopyToAsync.*/
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }
                FileModel file = new FileModel { Name = uploadedFile.FileName, Path = path};
                _context.Files.Add(file);
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }





    }
}
